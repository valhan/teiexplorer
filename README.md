![TEIExplorer](https://github.com/Valerie-Hanoka/TEIExplorer/blob/master/illustration.png)

An (dirty) unfinished tool to explore and compare XML/TEI French Litterature Corpora
========================================================================================

*In development*
---------------

When exploring a corpus of documents XML/TEI, one may need to
know what texts are similar, and on what basis.
This package intend to become a suite for text comparison.
It will allow to:

    - Read and store metadata information of each XML/TEI document
      E.g. :
         • Header metadata: Title, Author(s),...
         • Body metrics: #words, #sentences, polarity, ...
    - Run an unsupervised comparing tool for a set of texts (To be done).


This tool is being developed as part of the OBVIL projects.
See http://obvil.paris-sorbonne.fr/

---------------
